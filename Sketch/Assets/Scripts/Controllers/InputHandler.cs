﻿///
/// @author GusPassos
/// 

using Sketch.Battle;
using Sketch.Characters;
using Sketch.Characters.PlayableCharacters;
using UnityEngine;

namespace Sketch.Controllers
{
    /// <summary>
    /// Handles the player input
    /// </summary>
    public class InputHandler : MonoBehaviour
    {
        public static bool          hasControll = true;

        [SerializeField]
        private static CharController      charController;
        private static SketchChar          playableCharacter;
    
        private Vector2             directionalInput;

        // player's input
        private float keyHorizontal = 0;
        private float joyHorizontal = 0;
        private float keyVertical = 0;
        private float joyVertical = 0;

        public void Init(GameObject character)
        {
            SetCharacterScripts(character);
        }

        private void SetCharacterScripts(GameObject newCharacter)
        {
            charController      = newCharacter.GetComponent<CharController>();
            playableCharacter   = newCharacter.GetComponent<SketchChar>();
        }

        public static void EnableController(bool hasControll)
        {
            if(!hasControll)
            {
                charController.SetDirectionalInput(Vector2.zero);
            }

            InputHandler.hasControll = hasControll;
        }

        public Vector2 GetDirectionalInput()
        {
            return directionalInput;
        }

        private void Update()
        {
            if(hasControll)
            {
                //if (playableCharacter == null || charStatus.IsControllable())
                //{
                    HandleCharMovement();
                    HandleJump();

                    playableCharacter.ListenCommand();
                //}
            }
        }

        private void HandleCharMovement()
        {
            keyHorizontal   = Input.GetAxisRaw("key_Horizontal");
            keyVertical     = Input.GetAxisRaw("key_Vertical");

            if (Input.GetJoystickNames().Length > 0)
            {
                joyHorizontal   = Input.GetAxisRaw("joy_Horizontal");
                joyVertical     = Input.GetAxisRaw("joy_Vertical");
            }

            // if the joystick is being used
            if (joyHorizontal == 0 && joyVertical == 0)
            {
                // keyboard input
                directionalInput = new Vector2( keyHorizontal,
                                                keyVertical);
            }
            else
            {
                // joystick input
                directionalInput = new Vector2( joyHorizontal,
                                                joyVertical);
            }

           charController.SetDirectionalInput(directionalInput);
        }

        private void HandleJump()
        {
            if (Input.GetButtonDown("key_Jump") || Input.GetButtonDown("joy_Jump"))
            {
                charController.OnJumpInputDown();
            }
            else if (Input.GetButtonUp("key_Jump") || Input.GetButtonUp("joy_Jump"))
            {
                charController.OnJumpInputUp();
            }
        }
    }
}
