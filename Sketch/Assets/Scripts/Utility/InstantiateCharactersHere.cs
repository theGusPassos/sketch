﻿using Sketch.Camera;
using Sketch.ControllerFlow;
using UnityEngine;

namespace LastaMage.Utility
{
    /// <summary>
    /// Use this only for debugging
    /// </summary>
    public class InstantiateCharactersHere : MonoBehaviour
    {
        private CharManager playableCharManager;

        public void Start()
        {
            playableCharManager = GameObject.Find("ControllerFlow").GetComponent<CharManager>();

            GameObject sketch = Instantiate(
                playableCharManager.sketchPrefab,
                transform.position,
                transform.rotation
                );

            sketch.transform.localScale = transform.localScale;
            sketch.name = "Sketch";

            CharManager.SetSketchGameObject(sketch);

            SetCameraTargets();
        }

        private void SetCameraTargets()
        {
            CameraFollow.SetTarget(CharManager.sketchInstantiated.GetComponent<BoxCollider2D>());
        }
    }
}

