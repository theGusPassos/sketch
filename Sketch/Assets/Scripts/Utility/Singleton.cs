﻿///
/// @author GusPassos
/// 

using UnityEngine;

namespace Sketch.Utility
{
    public class Singleton : MonoBehaviour
    {
        private static bool created = false;

        private void Awake()
        {
            if(!created) created = true;
            else Destroy(gameObject);
        }
    }
}
