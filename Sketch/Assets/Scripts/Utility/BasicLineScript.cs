﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLineScript : MonoBehaviour {
    
    public Vector3[] positions;
    private int currentPos;

    private void Update ()
    {
		if (Vector3.Distance(transform.localPosition, positions[currentPos]) > 1)
        {
            transform.localPosition = Vector2.Lerp(transform.localPosition, positions[currentPos], 0.1f);
        }
        else
        {
            currentPos = (currentPos == 0) ? 1 : 0;
        }
	}
}
