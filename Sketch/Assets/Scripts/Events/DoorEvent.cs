﻿///
/// @author GusPassos
///

using Sketch.GameManager;
using UnityEngine;

namespace Sketch.Events
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class DoorEvent : MonoBehaviour
    {
        private static SceneLoader sceneLoader;

        public string nextScene;
        public int nextDoorId;

        private void Start ()
        {
            if ( sceneLoader == null )
            {
                sceneLoader = GameObject.Find ("SceneLoader").GetComponent<SceneLoader> ();
            }
        }

        private void OnTriggerStay2D ( Collider2D coll )
        {
            //if (charController.GetCharDistance() < 3)
           // {
                // sets the next door so the LevelEventManager knows where to place the player
                //LevelEventsManager.nextDoor = nextDoorId;
                sceneLoader.LoadScene ( nextScene );
            //}
        }
    }
}