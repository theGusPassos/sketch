﻿///
/// @author GusPassos
///

using Sketch.ControllerFlow;
using Sketch.Controllers;
using UnityEngine;

namespace LastaMage.Events
{
    public class DoorEventManager : MonoBehaviour
    {
        public static int       nextDoor;
        private static float    charDistance = 0.5f;

        public Transform[] doors;

        private void Awake ()
        {
            if (GameStarter.gameStarted)
                PlaceCharacterInDoor (nextDoor);
        }

        public void PlaceCharacterInDoor(int door)
        {
            CharManager.sketchInstantiated.transform.position = doors[door].position + new Vector3(charDistance, 0, 0);
        }
    }
}