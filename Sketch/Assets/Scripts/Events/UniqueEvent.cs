﻿///
/// @author GusPassos
///

using Sketch.Persistance;
using UnityEngine;

namespace Sketch.Events
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class UniqueEvent : MonoBehaviour
    {
        [HideInInspector]
        public UniqueEventsManager levelEventsManager;

        public string ID;

        [Tooltip("If the event will be executed as soon as instantiated")]
        public bool executeOnLoad = false;

        [Tooltip("Events that must have happened before this one")]
        public string[] requirements;
        [Tooltip("Events that can't have happened before this one")]
        public string[] unRequirements;

        private void Start()
        {
            if (executeOnLoad)
            {
                Execute();
            }
        }

        private void OnTriggerEnter2D(Collider2D coll)
        {
            DisableEventStart();

            Execute();
        }

        public abstract void Execute ();

        public void EndEvent ()
        {
            EventDatabase.SaveTemporaryEvent ( ID );
            // set the events again, since some event might be activated now that this one is completed
            try
            {
                levelEventsManager.SetEvents();
            }
            catch ( System.Exception e )
            {
                Debug.LogError("This event '" + ID + "' isn't registred in the level event manager. Exception: " + e);
            }

            Destroy ( gameObject );
        }

        public void SetEnableObject (bool enable)
        {
            gameObject.SetActive ( enable );
        }

        public bool GetEnableObject ()
        {
            return gameObject.activeSelf;
        }

        public void Destroy ()
        {
            Destroy ( gameObject );
        }

        protected void DisableEventStart()
        {
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}