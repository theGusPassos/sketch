﻿///
/// @author GusPassos
/// 

using Sketch.Controllers;
using Sketch.Persistance;
using UnityEngine;

// 
// Find out why the event isnt being turned on :)
// and create a camera manager....
namespace Sketch.Events
{
    public class UniqueEventsManager : MonoBehaviour
    {
        //public static int nextDoor = 0;
        
        //[SerializeField]
        //private Transform[] doors;

        [SerializeField]
        private UniqueEvent[] events;

        private void Start ()
        {
            if(GameStarter.gameStarted)
                SetEvents ();
        }

        // disable and enable events based on requires and unRequirements
        public void SetEvents ()
        {
            foreach ( UniqueEvent e in events )
            {
                if (e != null)
                {
                    if (EventDatabase.FindTemporaryEvent ( e.ID )) e.Destroy ();
                    else
                    {
                        bool active = true;
                        if (e.requirements != null)
                        {
                            foreach (string req in e.requirements)
                            {
                                if (!( active = EventDatabase.FindTemporaryEvent( req ) )) e.SetEnableObject ( false ); break;
                            }
                        }

                        if ( active )
                        {
                            if (e.unRequirements != null)
                            {
                                foreach (string unReq in e.unRequirements)
                                {
                                    if ((active = EventDatabase.FindTemporaryEvent( unReq ) )) e.Destroy(); active = !active; break;
                                }
                           }
                        }
                        if ( active )
                        {
                            e.levelEventsManager = this;
                            e.SetEnableObject ( true );
                        }
                    }
                }
            } // end foreach 
        }
    }
}