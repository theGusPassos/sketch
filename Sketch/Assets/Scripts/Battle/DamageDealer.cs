﻿///
/// @author GusPassos
/// 

using Sketch.Battle.DrawSystem;
using Sketch.Battle.HealthSystem;
using Sketch.Characters;
using UnityEngine;

namespace Sketch.Battle
{
    public class DamageDealer : MonoBehaviour
    {
        private static GameObject healthBarPrefab;
        private IHealthBar healthBar;
        private ICharacter character;

        public float    maxHealthPoints;
        private float   currentHealthPoints = 0;

        private void Awake()
        {
            if (healthBarPrefab == null)
                healthBarPrefab = (GameObject)Resources.Load("UI/Battle/HealthBars/BasicEnemyHealthBar");
        }

        private void Start()
        {
            character = GetComponent<ICharacter>();

            currentHealthPoints = maxHealthPoints;

            PreventErrors();
        }

        private void PreventErrors()
        {
            if (healthBarPrefab == null)
                Debug.LogError($"The static reference for health bar prefab isn't working: {gameObject.name}");

            if (character == null)
                Debug.LogError($"ICharacter in {gameObject.name} is null");
        }

        public void DealDamage(float intensity, AttackType type)
        {
            ShowHealthBar();

            float valueToGo = currentHealthPoints -= GetDamageDealt(intensity, type);
            healthBar.SetValueToGo(valueToGo);

            CheckDeath();
        }

        /// <summary>
        /// Some characters may have different ways of calculating the damage caused
        /// based on the attack type and intensity
        /// </summary>
        protected virtual float GetDamageDealt(float intensity, AttackType type)
        {
            return intensity;
        }

        private void CheckDeath()
        {
            if (currentHealthPoints <= 0)
            {
                healthBar.DisableHealthBar();
                character.Die();
            }
        }

        /// <summary>
        /// Instantiates the health bar in the game. Can be used for boss fights, since the 
        /// health bar, in this case, will be shown as soon as the boss appears
        /// </summary>
        public void ShowHealthBar()
        {
            if (healthBar == null)
            {
                healthBar = Instantiate(healthBarPrefab, transform).GetComponent<IHealthBar>();
                healthBar.Initialize(maxHealthPoints);
            }
        }

        /// <summary>
        /// Set a specific health bar
        /// </summary>
        public void SetHealthBar(IHealthBar healthBar)
        {
            this.healthBar = healthBar;
            this.healthBar.Initialize(maxHealthPoints);
        }
    }
}