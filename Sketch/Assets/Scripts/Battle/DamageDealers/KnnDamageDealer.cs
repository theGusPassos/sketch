﻿using Sketch.Battle.DrawSystem;

namespace Sketch.Battle.DamageDealers
{
    public class KnnDamageDealer : DamageDealer
    {
        protected override float GetDamageDealt(float intensity, AttackType type)
        {
            if (type == PredictAttackType(type))
                intensity = 0;
            
            return intensity;
        }

        private AttackType PredictAttackType(AttackType currentAttackType)
        {
            return AttackType.CIRCLE;
        }
    }
}
