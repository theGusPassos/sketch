﻿namespace Sketch.Battle.HealthSystem
{
    public interface IHealthBar
    {
        void Initialize(float maxValue);
        void SetValueToGo(float valueToGo);
        void DisableHealthBar();
    }
}
