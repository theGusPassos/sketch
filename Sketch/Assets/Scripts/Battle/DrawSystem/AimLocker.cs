﻿using Sketch.Camera;
using System.Collections.Generic;
using UnityEngine;

namespace Sketch.Battle.DrawSystem
{
    public class AimLocker : MonoBehaviour
    {
        private Vector3 aimStandardPos = new Vector3(3.5f, 0, 0);

        private List<Transform> targetsInRage = new List<Transform>();
        private int currentTarget = -1;

        [SerializeField]
        private GameObject aim;

        private void Awake()
        {
            aim = Instantiate(aim);
            aim.SetActive(false);
            aim.name = "Aim";
            aim.transform.position = GetAheadPosition();    
        }

        public Transform GetAimTransform()
        {
            return aim.transform;
        }

        private void Update()
        { 
            // the aim lock is on
            if (currentTarget >= 0)
            {
                if (currentTarget > targetsInRage.Count) return;

                aim.transform.position = targetsInRage[currentTarget].position;
            }
            else
            {
                aim.transform.position = GetAheadPosition();
            }
        }

        public void TryLockTarget()
        {
            // is locked already
            if (currentTarget >= 0)
            {
                UnlockTarget();
            }
            else
            { 
                LockTarget();
            }
        }

        private void LockTarget()
        {
            currentTarget = GetClosestTargetIndex();

            if (currentTarget != -1)
            {
                CameraFollow.SetEnemyTarget(targetsInRage[currentTarget]);
                aim.transform.position = targetsInRage[currentTarget].position;
                aim.SetActive(true);
            }
        }

        private void UnlockTarget()
        {
            aim.SetActive(false);
            CameraFollow.UnsetEnemyTarget();
            currentTarget = -1;
        }

        public void LockInNextTarget(int targetIndex)
        {
            if (targetsInRage.Count == 0)
                return;

            if (currentTarget == targetsInRage.Count - 1)
            {
                currentTarget = 0;
                return;
            }

            currentTarget += targetIndex;
        }

        private Vector3 GetAheadPosition()
        {
            return transform.position + aimStandardPos * transform.parent.localScale.x;
        }

        private int GetClosestTargetIndex()
        {
            if (targetsInRage.Count == 0) return -1;
            if (targetsInRage.Count == 1) return 0;

            float distance = float.MaxValue;
            float thisDist;

            int closest = -1;

            for (int i = 0; i < targetsInRage.Count; i++)
            {
                if ((thisDist = 
                    Vector2.Distance(targetsInRage[i].position, transform.position)) < distance)
                {
                    distance = thisDist;
                    closest = i;
                } 
            }

            return closest;
        }

        private void AddTarget(Transform target)
        {
            targetsInRage.Add(target);
        }

        private void RemoveTarget(Transform target)
        {
            int removed = targetsInRage.IndexOf(target);

            targetsInRage.Remove(target);

            if (removed == currentTarget)
            {
                if (targetsInRage.Count > 0)
                {
                    currentTarget = GetClosestTargetIndex();
                }
                else
                {
                    UnlockTarget();
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D obj)
        {
            AddTarget(obj.transform);
        }

        private void OnTriggerExit2D(Collider2D obj)
        {
            RemoveTarget(obj.transform);
        }
    }
}
