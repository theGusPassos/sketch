﻿using Assets.Scripts.Battle.DrawSystem.LineRenderers;
using UnityEngine;

namespace Sketch.Battle.DrawSystem.BrushRenderer
{
    public class BrushCreator : MonoBehaviour
    {
        [SerializeField] private GameObject slashRendererPrefab;
        [SerializeField] private GameObject verticalRendererPrefab;
        [SerializeField] private GameObject circleRendererPrefab;

        private Transform aimTransform;

        private bool readyToAttack = false;
        private float timer = 0;
        private float timeToWait = 0;

        public void SetAimTransform(Transform aimTransform)
        {
            this.aimTransform = aimTransform;
        }

        public void SetCurrentRenderer(AttackType type, float intensity)
        {
            if (readyToAttack)
            {
                if (type == AttackType.SLASH)
                {
                    timeToWait = 
                        Instantiate(slashRendererPrefab).GetComponent<BaseRenderer>().InitRenderer(intensity, aimTransform.position);
                }
                else if (type == AttackType.VERTICAL)
                {
                    timeToWait =
                        Instantiate(verticalRendererPrefab).GetComponent<BaseRenderer>().InitRenderer(intensity, aimTransform.position);
                }
                else if (type == AttackType.CIRCLE)
                {
                    timeToWait =
                        Instantiate(circleRendererPrefab).GetComponent<BaseRenderer>().InitRenderer(intensity, aimTransform.position);
                }

                readyToAttack = false;
                timer = 0;
            }
        }

        private void Update()
        {
            if (!readyToAttack)
            {
                timer += Time.deltaTime;

                if (timer > timeToWait)
                {
                    readyToAttack = true;
                }
            }
        }
    }
}