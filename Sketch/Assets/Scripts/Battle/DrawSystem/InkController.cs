﻿using Sketch.GameManager;
using UnityEngine;
using UnityEngine.UI;

namespace Sketch.Battle.DrawSystem
{
    public class InkController : MonoBehaviour
    {
        public GameObject inkBar;
        private Slider inkSlider;

        private float maxInkValue = 100;
        private float minInkValue = 0;

        private float valueToGo;
        private float changeConst = 0.05f;
        
        private void Awake()
        {
            SetImage();
        }

        private void SetImage()
        {
            inkBar = Instantiate(inkBar);

            MainCanvasManager.Instance.AddObjetToCanvas(inkBar);

            inkSlider = inkBar.GetComponentInChildren<Slider>();
            inkSlider.maxValue = maxInkValue;
            inkSlider.minValue = minInkValue;

            inkSlider.value = valueToGo = maxInkValue;

            inkBar = null;
        }

        private void Update()
        {
            if (!EqualFloats(valueToGo, inkSlider.value))
            {
                inkSlider.value = Mathf.Lerp(inkSlider.value, valueToGo, changeConst);
            }
        }

        private bool EqualFloats(float a, float b)
        {
            return (Mathf.Abs(a - b) < 0.0001f);
        }

        public bool HasEnoughInkFor(float inkRequirement)
        {
            return (inkSlider.value > inkRequirement);
        }

        public void AddInk(float add)
        {
            valueToGo += add;

            if (valueToGo > maxInkValue) valueToGo = maxInkValue;
        }

        public void SubtractFromInk(float subtract)
        {
            if (valueToGo < inkSlider.value)
            {
                valueToGo -= subtract;
            }
            else
            {
                valueToGo = inkSlider.value - subtract;
            }

            if (valueToGo < minInkValue) valueToGo = minInkValue;
        }
    }
}
