﻿using Sketch.Battle.DrawSystem.BrushRenderer;
using UnityEngine;

namespace Sketch.Battle.DrawSystem
{
    public class Drawer : MonoBehaviour
    {
        private InkController   inkController;
        private BrushCreator     brushDrawer;

        // current attack being charged
        private AttackType  chargedAttack;
        private float       timeCharging = 0;

        private float timeToAttack = 0.05f;
        private float timer = 0;

        private void Awake()
        {
            inkController = GetComponent<InkController>();
            brushDrawer = GetComponent<BrushCreator>();
        }

        public void SetAimTransfrom(Transform aimTransform)
        {
            brushDrawer.SetAimTransform(aimTransform);
        }

        private void Update()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    brushDrawer.SetCurrentRenderer(chargedAttack, timeCharging);
                    timeCharging = 0;
                    timer = 0;
                }
            }
        }

        private void SetTimerToRelease()
        {
            timer = timeToAttack;
        }

        private void AddChargeForce()
        {
            timeCharging += Time.deltaTime;
        }

        private void LateUpdate()
        {
            inkController.AddInk(DrawConstants.inkPerFrame * Time.deltaTime);
        }

        public void ChargeSlashAttack()
        {
            if (inkController.HasEnoughInkFor(DrawConstants.slashCost))
            {
                SetTimerToRelease();
                AddChargeForce();
                chargedAttack = AttackType.SLASH;

                inkController.SubtractFromInk(DrawConstants.slashCost);
            }
        }

        public void ChargeVertialAttack()
        {
            if (inkController.HasEnoughInkFor(DrawConstants.verticalCost))
            {
                SetTimerToRelease();
                AddChargeForce();
                chargedAttack = AttackType.VERTICAL;

                inkController.SubtractFromInk(DrawConstants.verticalCost);
            }
        }

        public void ChargeCircleAttack()
        {
            if (inkController.HasEnoughInkFor(DrawConstants.circleCost))
            {
                SetTimerToRelease();
                AddChargeForce();
                chargedAttack = AttackType.CIRCLE;

                inkController.SubtractFromInk(DrawConstants.circleCost);
            }
        }
    }

    public enum AttackType
    {
        SLASH       = 0,
        VERTICAL    = 1,
        CIRCLE      = 2,
        NONE        = 404
    }
}