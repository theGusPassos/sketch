﻿using Sketch.Battle.DrawSystem;
using System;
using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineRenderers
{
    public class SlashRenderer : BaseRenderer
    {
        private AttackIntensity intensity;

        private BasicStats currentSlashStats;
        private LineTimer currentLineTimer;

        private TrailRenderer trail;
        private bool rendering = false;

        [SerializeField] private float[] distanceByIntensity;
        private SlashVertices slashVertices;

        private int positionIndex;
        private int endPosition;

        protected override void Init(AttackIntensity attackIntensity)
        {
            this.intensity = attackIntensity;

            currentSlashStats = basicStats[(int)attackIntensity];
            currentLineTimer = lineTimers[(int)attackIntensity];
            slashVertices = new SlashVertices(distanceByIntensity[(int)attackIntensity]);

            StartTrail();
        }

        private void StartTrail()
        {
            trail = GetComponentInChildren<TrailRenderer>();
            trail.widthMultiplier = currentSlashStats.startWidth;
            trail.time = currentSlashStats.timeToEnd;

            SetStartPoint();

            rendering = true;
            GetComponentInChildren<AttackCollHandler>().StartCollider(AttackType.SLASH, intensity);
        }

        private void SetStartPoint()
        {
            positionIndex = UnityEngine.Random.Range(0, 2);
            endPosition = UnityEngine.Random.Range(0, 2);

            trail.transform.localPosition = slashVertices.points[positionIndex][(endPosition == 1) ? 0 : 1];
        }

        private void Update()
        {
            if (rendering)
            {
                ApplyTrailModifiers();

                if (timer < currentLineTimer.timeMoving)
                {
                    trail.transform.localPosition = Vector2.Lerp(trail.transform.localPosition, slashVertices.points[positionIndex][endPosition], currentSlashStats.speed);
                }

                if (timer > currentLineTimer.timeShowing)
                {
                    Destroy(gameObject);
                }

                timer += Time.deltaTime;
            }
        }

        private void ApplyTrailModifiers()
        {
            trail.widthMultiplier += currentSlashStats.widthMultiplier;

            Color trailColor = trail.material.GetColor("_TintColor");
            trailColor += new Color(0, 0, 0, currentSlashStats.alphaMultiplier);

            trail.material.SetColor("_TintColor", trailColor);
        }

        [Serializable]
        public struct SlashVertices
        {
            public Vector2[][] points;

            public SlashVertices(float distance)
            {
                points = new Vector2[2][];

                points[0] = new Vector2[2];
                points[0][0] = new Vector2(-distance, -distance);
                points[0][1] = new Vector2(distance, distance);

                points[1] = new Vector2[2];
                points[1][0] = new Vector2(distance, -distance);
                points[1][1] = new Vector2(-distance, distance);
            }
        }
    }
}
