﻿using Sketch.Battle.DrawSystem;
using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineRenderers
{
    public class VerticalRenderer : BaseRenderer
    {
        private AttackIntensity intensity;

        private BasicStats currentVerticalStats;
        private LineTimer currentLineTimer;

        private TrailRenderer trail;
        private bool rendering = false;

        [SerializeField] private Vector2[] positions;
        private int endPoint;

        protected override void Init(AttackIntensity attackIntensity)
        {
            this.intensity = attackIntensity;

            currentVerticalStats = basicStats[(int)attackIntensity];
            currentLineTimer = lineTimers[(int)attackIntensity];

            StartTrail();
        }

        private void StartTrail()
        {
            trail = GetComponentInChildren<TrailRenderer>();

            trail.widthMultiplier = currentVerticalStats.startWidth;
            trail.time = currentVerticalStats.timeToEnd;

            SetStartPoint();

            rendering = true;

            GetComponentInChildren<AttackCollHandler>().StartCollider(AttackType.VERTICAL, intensity);
        }

        private void SetStartPoint()
        {
            endPoint = Random.Range(0, 2);

            trail.transform.localPosition = positions[(endPoint == 1) ? 0 : 1];
        }

        private void Update()
        {
            if (rendering)
            {
                ApplyTrailModifiers();

                if (timer < currentLineTimer.timeMoving)
                {
                    trail.transform.localPosition = Vector2.Lerp(trail.transform.localPosition, positions[endPoint], currentVerticalStats.speed);
                }

                if (timer > currentLineTimer.timeShowing)
                {
                    Destroy(gameObject);
                }

                timer += Time.deltaTime;
            }
        }

        private void ApplyTrailModifiers()
        {
            trail.widthMultiplier += currentVerticalStats.widthMultiplier;

            Color trailColor = trail.material.GetColor("_TintColor");
            trailColor += new Color(0, 0, 0, currentVerticalStats.alphaMultiplier);

            trail.material.SetColor("_TintColor", trailColor);
        }
    }
}
