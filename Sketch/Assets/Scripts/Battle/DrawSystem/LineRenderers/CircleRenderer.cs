﻿using Sketch.Battle.DrawSystem;
using System;
using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineRenderers
{
    public class CircleRenderer : BaseRenderer
    {
        private AttackIntensity intensity;

        private BasicStats currentCircleStats;
        private LineTimer currentLineTimer;

        private TrailRenderer trail;
        private bool rendering = false;

        protected override void Init(AttackIntensity attackIntensity)
        {
            this.intensity = attackIntensity;

            currentCircleStats = basicStats[(int)attackIntensity];
            currentLineTimer = lineTimers[(int)attackIntensity];

            StartTrail();
        }

        private void StartTrail()
        {
            trail = GetComponentInChildren<TrailRenderer>();
            trail.widthMultiplier = currentCircleStats.startWidth;
            trail.time = currentCircleStats.timeToEnd;

            rendering = true;

            GetComponentInChildren<AttackCollHandler>().StartCollider(AttackType.CIRCLE, intensity);
        }

        private void Update()
        {
            if (rendering)
            {
                ApplyTrailModifiers();

                if (timer < currentLineTimer.timeMoving)
                {
                    transform.Rotate(new Vector3(0, 0, currentCircleStats.speed) * Time.deltaTime);
                }

                if (timer > currentLineTimer.timeShowing)
                {
                    Destroy(gameObject);
                }

                timer += Time.deltaTime;
            }
        }

        private void ApplyTrailModifiers()
        {
            trail.widthMultiplier += currentCircleStats.widthMultiplier;
            
            Color trailColor = trail.material.GetColor("_TintColor");
            trailColor += new Color(0, 0, 0, currentCircleStats.alphaMultiplier);

            trail.material.SetColor("_TintColor", trailColor);
        }
    }
}
