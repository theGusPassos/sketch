﻿using System;
using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineRenderers
{
    public class BaseRenderer : MonoBehaviour
    {
        [SerializeField] private IntensityTypeStats[] attackStats;
        [SerializeField] protected BasicStats[] basicStats;
        [SerializeField] protected LineTimer[] lineTimers;

        private AttackIntensity attackIntensity;

        protected float timer = 0;

        public float InitRenderer(float intensity, Vector3 startPosition)
        {
            transform.position = startPosition;

            foreach (IntensityTypeStats i in attackStats)
            {
                if (intensity < i.intensityRequired)
                {
                    attackIntensity = i.type;
                    Init(attackIntensity);

                    return i.timeToWait;
                }
            }

            Init(attackIntensity);
            return attackStats[attackStats.Length].timeToWait;
        }

        protected virtual void Init(AttackIntensity attackIntensity) { }
    }

    [Serializable]
    public struct LineTimer
    {
        public float timeMoving;
        public float timeShowing;
    }

    [Serializable]
    public struct IntensityTypeStats
    {
        public AttackIntensity type;
        public float intensityRequired;
        public float timeToWait;
    }

    [Serializable]
    public struct BasicStats
    {
        public float startWidth;
        public float widthMultiplier;
        public float alphaMultiplier;
        public float timeToEnd;
        public float speed;
    }

    public enum AttackIntensity
    {
        NORMAL          = 0,
        HALF_CHARGED    = 1,
        CHARGED         = 2
    }
}
