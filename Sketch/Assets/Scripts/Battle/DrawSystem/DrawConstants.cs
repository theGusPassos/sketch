﻿namespace Sketch.Battle.DrawSystem
{
    public class DrawConstants
    {
        // inputs names in editor
        public static string slashAttack       = "slashAttack";
        public static string verticalAttack    = "verticalAttack";
        public static string circleAttack      = "circleAttack";

        // values as percentage
        public static float inkPerFrame    = 10;
        public static float slashCost      = 1;
        public static float verticalCost   = 1;
        public static float circleCost     = 1;

        public static float basicIntensity          = 1;
        public static float halfChargedIntensity    = 2;
        public static float chargedIntensity         = 3;
    }
}