﻿using Assets.Scripts.Battle.DrawSystem.LineRenderers;
using UnityEngine;

namespace Sketch.Battle.DrawSystem
{
    public class AttackCollHandler : MonoBehaviour
    {
        private BoxCollider2D coll;

        private AttackType type;
        private float intensityLevel;

        private void Awake()
        {
            coll = GetComponent<BoxCollider2D>();
        }

        public void StartCollider(AttackType type, AttackIntensity intensity)
        {
            this.type = type;

            if (intensity == AttackIntensity.CHARGED)
            {
                intensityLevel = 10;
            }
            else if (intensity == AttackIntensity.HALF_CHARGED)
            {
                intensityLevel = 5;
            }
            else
            {
                intensityLevel = 2.5f;
            }

            coll.enabled = true;
        }

        private void OnTriggerEnter2D(Collider2D coll)
        {
            coll.GetComponent<DamageDealer>().DealDamage(intensityLevel, type);
        }
    }
}

