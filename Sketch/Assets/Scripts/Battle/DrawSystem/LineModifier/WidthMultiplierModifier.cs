﻿using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineModifier
{
    public class WidthMultiplierModifier : ILineModifier
    {
        private readonly float modifier;

        public WidthMultiplierModifier(float modifier)
        {
            this.modifier = modifier;
        }

        public void Modify(TrailRenderer trailRenderer)
        {
            trailRenderer.widthMultiplier += modifier;
        }
    }
}
