﻿using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineModifier
{
    public interface ILineModifier
    {
        void Modify(TrailRenderer trailRenderer);
    }
}
