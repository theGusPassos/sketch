﻿using UnityEngine;

namespace Assets.Scripts.Battle.DrawSystem.LineModifier
{
    public class MaterialColorModifier : ILineModifier
    {
        private readonly Color modifier;
        private Color currentColor;

        public MaterialColorModifier(Color modifier)
        {
            this.modifier= modifier;
        }

        public void Modify(TrailRenderer trailRenderer)
        {
            currentColor = trailRenderer.material.GetColor("_TintColor");
            currentColor += modifier;

            trailRenderer.material.SetColor("_TintColor", currentColor);
        }
    }
}
