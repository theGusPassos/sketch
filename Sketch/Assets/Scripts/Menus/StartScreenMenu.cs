﻿///
/// @author GusPassos
///

using Sketch.Controllers;
using UnityEngine;

namespace Sketch.Menus
{
    public class StartScreenMenu : MonoBehaviour
    {
        [HideInInspector]
        public GameStarter gameStarter;

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.S))
            {
                gameStarter.StartGame();
            }
            if(Input.GetKeyDown(KeyCode.E))
            {

            }
        }
    }
}
