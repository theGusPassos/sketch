﻿///
/// @author GusPassos
///

using UnityEngine;

namespace Sketch.Camera
{
    /// <summary>
    /// Sets the camera are from this current scene
    /// </summary>
    public class CameraAreaSetter : MonoBehaviour
    {
        // HACK: in a single scene, there might be different camera areas and sessions
        // we should test which one to start at
        public Vector2      cameraArea;
        public GameObject   gameStartSession;

        private void Start()
        {
            if (cameraArea == Vector2.zero || gameStartSession == null)
                throw new System.Exception($"Variables in Camera Area Setter gameobject: {gameObject.name} weren't set");

            Bounds bounds = CameraBoundsCalculator.GetBounds(gameStartSession);

            CameraLimits.SetSceneBounds(bounds.max, bounds.min);
            CameraFollow.SetNewAreaSize(cameraArea);
        }
    }
}


