﻿using UnityEngine;

namespace Sketch.Camera
{
    public static class CameraBoundsCalculator
    {
        public static Bounds GetBounds(GameObject g)
        {
            var b = new Bounds(g.transform.position, Vector3.zero);
            foreach (Renderer r in g.GetComponentsInChildren<Renderer>())
            {
                b.Encapsulate(r.bounds);
            }
            return b;
        }
    }
}
