﻿///
/// @author GusPassos
/// 

using Sketch.Controllers;
using UnityEngine;

namespace Sketch.Camera
{
    /// <summary>
    /// Follows the player respecting a focus area
    /// </summary>
    public class CameraFollow : MonoBehaviour
    {
        private static bool instatiated = false;
        private static bool started = false;

        public static BoxCollider2D  target;
        public static InputHandler   player;

        // while aim locking another target
        private static bool aimLocked = false;
        private static Transform enemyTarget;

        // handle change in camera perspection
        private bool changingPerspective = false;

        // change in every scene
        private static FocusArea    focusArea;     
        private static Vector2      focusAreaSize;

        private Vector3 futurePosition;
        public float    verticalOffset;
        public float    lookAheadDstX;
        public float    lookSmoothTime;
        public float    verticalSmoothTime;

        private float   currentLookAheadX;
        private float   targetLookAheadX;
        private float   lookAheadDirX;
        private float   smoothLookVelocityX;
        private float   smoothVelocityY;
        private bool    lookAheadStopped = false;

        private CameraLimits cameraLimits;

        private void Awake()
        {
            if(!instatiated)
            {
                cameraLimits = GetComponent<CameraLimits>();

                instatiated = true;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public static void SetTarget(BoxCollider2D collider)
        {
            target = collider;
            focusArea = new FocusArea(target.bounds, focusAreaSize);
        }

        public static void AddPlayer(InputHandler playerToAdd)
        {
            player = playerToAdd;
        }

        /// <summary>
        /// Must be called in every new scene
        /// </summary>
        /// <param name="newFocusAreaSize"></param>
        public static void SetNewAreaSize(Vector2 newFocusAreaSize)
        {
            focusAreaSize = newFocusAreaSize;
            focusArea = new FocusArea(target.bounds, focusAreaSize);
        }

        public static void StartCamera()
        {
            started = true;
        }

        private void LateUpdate()
        {
            if (started)
            {
                if (!aimLocked)
                {
                    ControllCamera();
                }
                else
                {
                    ControllCameraWhileAiming();
                }
            }
        }

        private void ControllCamera()
        {
            Vector2 focusPosition = focusArea.center + Vector2.up * verticalOffset;

            focusArea.Update(target.bounds);

            if (focusArea.velocity.x != 0)
            {
                lookAheadDirX = Mathf.Sign(focusArea.velocity.x);

                if (Mathf.Sign(player.GetDirectionalInput().x) == Mathf.Sign(focusArea.velocity.x) &&
                    player.GetDirectionalInput().x != 0)
                {
                    targetLookAheadX = lookAheadDirX * lookAheadDstX;
                    lookAheadStopped = false;
                }
                else
                {
                    if (!lookAheadStopped)
                    {
                        targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4f;
                        lookAheadStopped = true;
                    }
                }
            }

            currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTime);

            focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
            focusPosition += Vector2.right * currentLookAheadX;
            futurePosition = (Vector3)focusPosition + Vector3.forward * -10;

            if (!CameraLimits.isChanging)
            {
                transform.position = futurePosition;
            }

            cameraLimits.CheckCameraOutbounds();
        }

        private void ControllCameraWhileAiming()
        {
            transform.position = Vector3.Lerp(target.transform.position, enemyTarget.position, 1);
        }

        public static void SetEnemyTarget(Transform enemy)
        {
            //aimLocked = true;
            //enemyTarget = enemy;
        }

        public static void UnsetEnemyTarget()
        {
            aimLocked = false;
            enemyTarget = null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 0, 0.4f);
            Gizmos.DrawCube(focusArea.center, focusAreaSize);
        }

        private struct FocusArea
        {
            public Vector2 center;
            public Vector2 velocity;
            float left;
            float right;
            float top;
            float bottom;

            public FocusArea(Bounds targetBounds, Vector2 size)
            {
                velocity = Vector2.zero;

                left = targetBounds.center.x - size.x / 2;
                right = targetBounds.center.x + size.x / 2;
                bottom = targetBounds.min.y;
                top = targetBounds.min.y + size.y;

                center = new Vector2((left + right) / 2, (top + bottom) / 2);
            }

            public void Update(Bounds targetBounds)
            {
                float shiftX = 0;
                if (targetBounds.min.x < left)
                {
                    shiftX = targetBounds.min.x - left;
                }
                else if (targetBounds.max.x > right)
                {
                    shiftX = targetBounds.max.x - right;
                }
                left += shiftX;
                right += shiftX;

                float shiftY = 0;
                if (targetBounds.min.y < bottom)
                {
                    shiftY = targetBounds.min.y - bottom;
                }
                else if (targetBounds.max.y > top)
                {
                    shiftY = targetBounds.max.y - top;
                }
                top += shiftY;
                bottom += shiftY;
                center = new Vector2((left + right) / 2, (top + bottom) / 2);
                velocity = new Vector2(shiftX, shiftY);
            }
        }
    }
}
