﻿using UnityEngine;

namespace Sketch.Camera
{
    public class CameraLimits : MonoBehaviour
    {
        private static SceneBounds sceneBounds;

        private static float xModifier = (13.35f / 10);
        private static float yModifier = (10 / 10);

        public static bool      isChanging = false;
        private static Vector3  valueToGo;
    
        private static bool     shouldChangeSize = false;
        private static float    sizeSign = 0;

        public static void SetSceneBounds(Vector2 max, Vector2 min)
        {
            sceneBounds.SetBounds(max, min);

            isChanging = true;
        }

        public void CheckCameraOutbounds()
        {
            if (isChanging == true)
            {
                if (valueToGo == Vector3.zero)
                {
                    valueToGo = GetFixedValue();
                }

                transform.position = new Vector3(
                    Mathf.Lerp(transform.position.x, valueToGo.x, 0.2f),
                    Mathf.Lerp(transform.position.y, valueToGo.y, 1f),
                    transform.position.z);

                if (transform.position.x == valueToGo.x ||
                    (transform.position.x < valueToGo.x + 0.1f && transform.position.x > valueToGo.x - 0.1f))
                {
                    isChanging = false;
                    valueToGo = Vector3.zero;
                }
            }
            else
            {
                transform.position = GetFixedValue();
            }

            if (shouldChangeSize)
            {
                ChangeCameraSize();
            }
        }

        private Vector3 GetFixedValue()
        {
            float x = transform.position.x;
            float y = transform.position.y;

            bool xChanged = false;
            bool yChanged = false;

            if (x > sceneBounds.maxX)
            {
                x = sceneBounds.maxX;
                xChanged = true;
            }

            if (x < sceneBounds.minX)
            {
                x = sceneBounds.minX;

                if (xChanged)
                    shouldChangeSize = true;

                xChanged = true;
            }

            if (y > sceneBounds.maxY)
            {
                y = sceneBounds.maxY;
                yChanged = true;
            }

            if (y < sceneBounds.minY)
            {
                y = sceneBounds.minY;

                if (yChanged)
                    shouldChangeSize = true;

                yChanged =  true;
            }

            //if (!yChanged && !xChanged && UnityEngine.Camera.main.orthographicSize < maxCameraSize)
            //{
            //    shouldChangeSize = true;
            //    sizeSign = 1;
            //}
            //else if (shouldChangeSize)
            //{
            //    if (!yChanged && !xChanged )
            //        shouldChangeSize = false;
            //    else
            //        sizeSign = -1;
            //}

            return new Vector3(x, y, transform.position.z);
        }

        private void ChangeCameraSize()
        {
            UnityEngine.Camera.main.orthographicSize += sizeSign * Time.deltaTime * 1;
            //sceneBounds.UpdateBounds();
        }

        [System.Serializable]
        private struct SceneBounds
        {
            public float maxX;
            public float minX;
            public float maxY;
            public float minY;

            public void SetBounds(Vector2 max, Vector2 min)
            {
                float cameraSize = UnityEngine.Camera.main.orthographicSize;

                maxX = max.x - xModifier * cameraSize;
                minX = min.x + xModifier * cameraSize;

                maxY = max.y - yModifier * cameraSize;
                minY = min.y + yModifier * cameraSize;
            }

            public void UpdateBounds()
            {
                float cameraSize = UnityEngine.Camera.main.orthographicSize;

                maxX = maxX - xModifier * cameraSize;
                minX = minX + xModifier * cameraSize;

                maxY = maxY - yModifier * cameraSize;
                minY = minY + yModifier * cameraSize;
            }
        }
    }
}