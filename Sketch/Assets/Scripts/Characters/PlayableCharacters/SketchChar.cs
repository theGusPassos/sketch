﻿///
/// @author GusPassos
/// 

using Sketch.Battle.DrawSystem;
using Sketch.GameManager;
using UnityEngine;

namespace Sketch.Characters.PlayableCharacters
{
    public class SketchChar : MonoBehaviour, ICharacter
    {
        [SerializeField]
        private GameObject      aimer;

        [SerializeField]
        private GameObject      drawer;

        private AimLocker       aimLocker;
        private Drawer          attacker;

        private void Awake()
        {
            aimLocker = Instantiate(aimer, transform).GetComponent<AimLocker>();
            aimLocker.transform.localPosition = Vector3.zero;

            attacker = Instantiate(drawer).GetComponent<Drawer>();
        }

        private void Start()
        {
            attacker.SetAimTransfrom(aimLocker.GetAimTransform());
        }
        
        public void ListenCommand()
        {
            TryAttackCommands();
            TryAimCommands();
        }

        private void TryAttackCommands()
        {
            if (Input.GetButton(DrawConstants.slashAttack))
            {
                attacker.ChargeSlashAttack();
            }
            else if (Input.GetButton(DrawConstants.verticalAttack))
            {
                attacker.ChargeVertialAttack();
            }
            else if (Input.GetButton(DrawConstants.circleAttack))
            {
                attacker.ChargeCircleAttack();
            }
        }

        private void TryAimCommands()
        {
            if (Input.GetButtonDown("lockTarget"))
            {
                aimLocker.TryLockTarget();
            }
            else if (Input.GetButtonDown("nextTarget"))
            {
                aimLocker.LockInNextTarget(1);
            }
            else if (Input.GetButtonDown("previousTarget"))
            {
                aimLocker.LockInNextTarget(-1);
            }
        }

        public void Die()
        {
            GameOverManager.instance.NotifyDeath();
        }
    }
}
