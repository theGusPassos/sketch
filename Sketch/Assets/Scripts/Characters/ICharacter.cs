﻿///
/// @author GusPassos
///

namespace Sketch.Characters
{
    public interface ICharacter
    {
        void Die();
    }
}