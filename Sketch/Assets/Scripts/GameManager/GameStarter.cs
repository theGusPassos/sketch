﻿///
/// @author GusPassos
/// 

using Sketch.ControllerFlow;
using Sketch.Events;
using Sketch.GameManager;
using Sketch.Menus;
using Sketch.Persistance;
using UnityEngine;

namespace Sketch.Controllers
{
    /// <summary>
    /// God object that will set everything
    /// </summary>
    public class GameStarter : MonoBehaviour
    {
        public static bool          gameStarted = false;

        [Tooltip("For fast debug. Doesn't work for multiplayer")]
        public bool                 startScreenEnabled = true;

        private GameProgressLoader  gameProgress;
        public GameObject           startScreen;
        public GameObject           controllerFlow;
        public GameObject           dialogueSystem;
        public GameObject           gameOverManager;
        public GameObject           barCanvas;

        private void Awake()
        {
            if (gameStarted)
            {
                Destroy(gameObject);
            }
            else
            {
                gameProgress = new GameProgressLoader();

                PlaceSceneLoader();
                PlaceControllerFlow();
                PlaceDialogueSystem();
                StartGameOverManager();
                StartBarCanvas();

                if (gameProgress.IsFirstTimePlaying())
                {
                    SceneLoader.sceneLoader.LoadFirstScene();
                }
                else
                {
                    SceneLoader.sceneLoader.LoadScene(gameProgress.GetCurrentScene());
                }
            }
        }

        private void Start()
        {
            // for debugging
            if (startScreenEnabled)
            {
                StartStartScreen();
            }
            else
            {
                StartGame();
            }
        }

        private void PlaceSceneLoader()
        {
            GameObject sceneLoader = new GameObject("SceneLoader");
            sceneLoader.AddComponent<SceneLoader>();
        }

        private void PlaceControllerFlow()
        {
            controllerFlow = Instantiate(controllerFlow);
            controllerFlow.name = "ControllerFlow";
        }

        private void PlaceDialogueSystem()
        {
            dialogueSystem = Instantiate(dialogueSystem);
            dialogueSystem.name = "Dialogue System";
        }

        public void StartStartScreen()
        {
            startScreen = Instantiate(startScreen);
            startScreen.name = "StartScreen";
            startScreen.GetComponent<StartScreenMenu>().gameStarter = this;
        }

        public void StartGameOverManager()
        {
            gameOverManager = Instantiate(gameOverManager);
            gameOverManager.name = "GameOverManager";
        }

        public void StartBarCanvas()
        {
            Instantiate(barCanvas).name = "BarCanvas";
        }

        /// <summary>
        /// Called to start the game
        /// </summary>
        public void StartGame()
        {
            PlayerManager.InitPlayers();

            InitializeEvents();

            if (startScreen.activeInHierarchy)
                Destroy(startScreen);

            gameStarted = true;

            Destroy(gameObject);
        }
        
        private void InitializeEvents()
        {
            GameObject.Find("LevelInfo").GetComponent<UniqueEventsManager>().SetEvents();
        }
    }
}
