﻿using UnityEngine;

namespace Sketch.GameManager
{
    public class MainCanvasManager : MonoBehaviour
    {
        public static MainCanvasManager Instance { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void AddObjetToCanvas(GameObject obj)
        {
            obj.name = "Ink Bar";
            obj.transform.SetParent(gameObject.transform);
            obj.GetComponent<RectTransform>().localPosition = Vector2.zero;
        }
    }
}
