﻿///
/// @author GusPassos
/// 

using UnityEngine;
using UnityEngine.UI;

namespace Sketch.UI
{
    public class GameOverUI : MonoBehaviour
    {
        public Text element;
        private bool fadingIn = false;
        private float fadeSpeed = 0.5f;

        private void Update()
        {
            if(fadingIn)
            {
                element.color += new Color(0, 0, 0, fadeSpeed * Time.deltaTime);
            }
        }

        public void ShowText()
        {
            fadingIn = true;
        }
    }
}