﻿///
/// @author GusPassos
/// 

using Sketch.Camera;
using UnityEngine;

namespace Sketch.ControllerFlow
{
    /// <summary>
    /// Character Placer for the game start
    /// </summary>
    public class CharPlacer : MonoBehaviour
    {
        private static bool firstPlacement = true;
        private CharManager playableCharManager;

        private void Awake()
        {
            playableCharManager = GetComponent<CharManager>();
        }

        /// <summary>
        /// Every starter scene has a ScenePCPosition that calls this passing the right position
        /// </summary>
        public void PlaceCharacters(CharacterPosStatus magePos)
        {
            if (firstPlacement)
            {
                InstantiateCharacters(magePos);
            }
            else
            {
                // this doesnt make sense for now
            }

            SetCameraTargets();
        }

        private void SetCameraTargets()
        {
            CameraFollow.SetTarget(CharManager.sketchInstantiated.GetComponent<BoxCollider2D>());
        }

        private void InstantiateCharacters(CharacterPosStatus sketchPos)
        {
            if (sketchPos.transform != null)
            {
                GameObject sketch = Instantiate(
                    playableCharManager.sketchPrefab,
                    sketchPos.transform.position,
                    sketchPos.transform.rotation
                    );

                sketch.transform.localScale = sketchPos.transform.localScale;
                sketch.name = "Sketch";

                CharManager.SetSketchGameObject(sketch);
            }
        }
    }
}
