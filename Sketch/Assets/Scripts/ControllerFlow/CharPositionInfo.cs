﻿///
/// @author GusPassos
/// 

using System;
using UnityEngine;

namespace Sketch.ControllerFlow
{
    /// <summary>
    /// Sends the playable character's position in this scene
    /// </summary>
    public class CharPositionInfo : MonoBehaviour
    {
        private static CharPlacer   placer;
        private static bool         hasPlaced = false;

        public CharacterPosStatus   sketchPos;

        private void Awake()
        {
            if (placer == null)
                placer = GameObject.Find("ControllerFlow").GetComponent<CharPlacer>();

            if(!hasPlaced)
            {
                placer.PlaceCharacters(sketchPos);
                hasPlaced = true;
            }
        }
    }

    [Serializable]
    public struct CharacterPosStatus
    {
        public Transform    transform;
        public Pose         pose;
    }

    public enum Pose
    {
        STANDING
    }
}
