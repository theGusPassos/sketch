﻿///
/// @author GusPassos
///

using Sketch.Camera;
using Sketch.Controllers;
using UnityEngine;

namespace Sketch.ControllerFlow
{
    public class CharManager : MonoBehaviour
    {
        public GameObject sketchPrefab;
        [HideInInspector]
        public static GameObject        sketchInstantiated;
        public static CharController    sketchController;

        public static void SetSketchGameObject(GameObject playableCharacter)
        {
            sketchInstantiated = playableCharacter;
            sketchController = sketchInstantiated.GetComponent<CharController>();

            CameraFollow.SetTarget(sketchInstantiated.GetComponent<BoxCollider2D>());
        }
    }
}
