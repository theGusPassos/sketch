﻿///
/// @author GusPassos
/// 

using Sketch.Camera;
using Sketch.Controllers;
using UnityEngine;

namespace Sketch.ControllerFlow
{
    /// <summary>
    /// Can delete and add new players
    /// </summary>
    public class PlayerManager : MonoBehaviour
    {
        /// <summary>
        /// Called once in the game initialization
        /// </summary>
        /// <param name="nPlayers"></param>
        public static void InitPlayers()
        {
            AddPlayer();

            CameraFollow.StartCamera();
        }

        private static void AddPlayer()
        {
            GameObject newPlayer = new GameObject("Player");
            DontDestroyOnLoad(newPlayer);

            newPlayer.AddComponent<InputHandler>();
            InputHandler playerInputHandler = newPlayer.GetComponent<InputHandler>();

            playerInputHandler.Init(CharManager.sketchInstantiated);
            //player = playerInputHandler;

            CameraFollow.AddPlayer(playerInputHandler);
        }
    }
}